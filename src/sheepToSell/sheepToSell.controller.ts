import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { SheepToSellService } from "./sheepToSell.service";
import { SheepToSellControllerBase } from "./base/sheepToSell.controller.base";

@swagger.ApiTags("sheepToSells")
@common.Controller("sheepToSells")
export class SheepToSellController extends SheepToSellControllerBase {
  constructor(
    protected readonly service: SheepToSellService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
