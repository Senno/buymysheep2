import * as common from "@nestjs/common";
import * as graphql from "@nestjs/graphql";
import * as nestAccessControl from "nest-access-control";
import { GqlDefaultAuthGuard } from "../auth/gqlDefaultAuth.guard";
import * as gqlACGuard from "../auth/gqlAC.guard";
import { SheepToSellResolverBase } from "./base/sheepToSell.resolver.base";
import { SheepToSell } from "./base/SheepToSell";
import { SheepToSellService } from "./sheepToSell.service";

@graphql.Resolver(() => SheepToSell)
@common.UseGuards(GqlDefaultAuthGuard, gqlACGuard.GqlACGuard)
export class SheepToSellResolver extends SheepToSellResolverBase {
  constructor(
    protected readonly service: SheepToSellService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
