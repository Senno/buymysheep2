import { Injectable } from "@nestjs/common";
import { PrismaService } from "nestjs-prisma";
import { SheepToSellServiceBase } from "./base/sheepToSell.service.base";

@Injectable()
export class SheepToSellService extends SheepToSellServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
