import { Module } from "@nestjs/common";
import { SheepToSellModuleBase } from "./base/sheepToSell.module.base";
import { SheepToSellService } from "./sheepToSell.service";
import { SheepToSellController } from "./sheepToSell.controller";
import { SheepToSellResolver } from "./sheepToSell.resolver";

@Module({
  imports: [SheepToSellModuleBase],
  controllers: [SheepToSellController],
  providers: [SheepToSellService, SheepToSellResolver],
  exports: [SheepToSellService],
})
export class SheepToSellModule {}
